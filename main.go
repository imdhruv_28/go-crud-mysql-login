package main

import (
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

func main() {

	fmt.Println("Server Started.")

	connectDatabase()

	route := mux.NewRouter()

	addApproutes(route)

	log.Fatal(http.ListenAndServe(":11000", route))

}
