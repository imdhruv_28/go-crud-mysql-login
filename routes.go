package main

import (
	"fmt"

	"github.com/gorilla/mux"
)

func addApproutes(route *mux.Router) {

	route.HandleFunc("/", homePage)

	route.HandleFunc("/signup", signupPage)

	route.HandleFunc("/login", loginPage)

	route.HandleFunc("/welcome", welcomePage)

	route.HandleFunc("/update", updateData)

	route.HandleFunc("/delete", deleteData)

	route.HandleFunc("/logout", logoutPage)

	fmt.Println("Routes are Loded.")
}
