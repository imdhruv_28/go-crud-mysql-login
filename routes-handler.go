package main

import (
	"net/http"
	"html/template"
	"github.com/gorilla/sessions"
	"log"
	"fmt"
	"github.com/gorilla/mux"
)

var store = sessions.NewCookieStore([]byte("mysession"))

func homePage(response http.ResponseWriter, request *http.Request) {
	http.ServeFile(response, request, "templates/index.html")
}


func signupPage(response http.ResponseWriter, request *http.Request) {
	if request.Method != "POST" {
		http.ServeFile(response, request, "templates/signup.html")
		return
	}
	username :=  request.FormValue("username")
	password :=  request.FormValue("password")

	_, err = db.Exec("INSERT INTO users(username, password) VALUES(?, ?)", username, password)
	if err != nil {
		http.Redirect(response, request, "/signup", 301)
		return
	}	else {
		http.Redirect(response, request, "/login", 200)
	}
}

func loginPage(response http.ResponseWriter, request *http.Request){
	if request.Method != "POST" {
		http.ServeFile(response, request, "templates/login.html")
		return
	}
	
	username :=  request.FormValue("username")
	password :=  request.FormValue("password")

	session, _ := store.Get(request, "mysession")
	session.Values["username"] = username
	session.Save(request, response)

	var database_username string
	var database_password string
	err := db.QueryRow("SELECT username, password FROM users WHERE username=?", username).Scan(&database_username, &database_password)
	if err != nil {
		http.Redirect(response, request, "/login", 301)
		return
	}	else if database_password != password {
		http.Redirect(response, request, "/login", 301)
		return
	}	else {
		http.Redirect(response, request, "/welcome", 301)
	}

}

func welcomePage(response http.ResponseWriter, request *http.Request) {
	session, _ := store.Get(request, "mysession")
	username := session.Values["username"]
	id := session.Values["id"]
	data := map[string] interface{} {
		"username": username,
		"id": id,
	}
	tmp, _ := template.ParseFiles("templates/welcome.html")
	tmp.Execute(response, data)
}

func updateData(response http.ResponseWriter, request *http.Request) {
	var userDetails User
	stmt, err := db.Prepare("UPDATE users SET username=?,password=? WHERE id=?")
	if err != nil {
		log.Fatal("Failed To Delete Data", err)
		return 
	}
	_, queryError := stmt.Exec(userDetails.id,userDetails.username, userDetails.password)
	if queryError != nil {
		log.Fatal(queryError)
		return 
	}	else {
		http.Redirect(response, request, "/login", 301)
	}
}

func deleteData(response http.ResponseWriter, request *http.Request) {
	userid := mux.Vars(request)["id"]
	stmt, err := db.Prepare("DELETE FROM users WHERE id=?")
	if err != nil {
		fmt.Println(err)
		return 
	}
	_, queryError := stmt.Exec(userid)
	if queryError != nil {
		fmt.Println(queryError)
		return 
	}	else {
		http.Redirect(response, request, "/login", 301)
	}
}

func logoutPage(response http.ResponseWriter, request *http.Request) {
	session, _ := store.Get(request, "mysession")
	session.Options.MaxAge = -1
	err = session.Save(request, response)
	if err != nil {
		log.Fatal("Failed To Delete Session", err)
	}
	http.ServeFile(response, request, "templates/login.html")
}

