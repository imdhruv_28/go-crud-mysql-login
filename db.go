package main

import (
	"database/sql"
	"fmt"

	_ "github.com/go-sql-driver/mysql"
)

var db *sql.DB
var err error

func connectDatabase() {
	db, err = sql.Open("mysql", "root:root@tcp(localhost:3306)/test")
	fmt.Println("Database connected.")
}
